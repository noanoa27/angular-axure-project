import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';

import { ProjectnullRoutingModule } from './projectnull-routing.module';
import { ProjectnullPageComponent } from './projectnull-page/projectnull-page.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    ProjectnullRoutingModule
  ],
  declarations: [ProjectnullPageComponent]
})
export class ProjectnullModule { }
