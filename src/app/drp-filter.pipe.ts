import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'drpFilter'
})
export class DrpFilterPipe implements PipeTransform {
  transform(items: any[], filter: string): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => {
      if(filter == 'Improvement' && item.improvement){
        return true;
      }else if(filter == 'Conservation' && item.conservation){
        return true;
      }else if(filter == 'All'){
        return true;
      }
    });
  }

}
