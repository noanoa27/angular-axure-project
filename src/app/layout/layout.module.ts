import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { Ng2CompleterModule } from "ng2-completer";

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    Ng2CompleterModule
  ],
  declarations: [TopmenuComponent],
  exports: [
    TopmenuComponent
  ]
})
export class LayoutModule { }
