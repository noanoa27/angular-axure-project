import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { Project } from 'src/app/model/project.model';

import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-brief-page',
  templateUrl: './brief-page.component.html',
  styleUrls: ['./brief-page.component.css']
})
export class BriefPageComponent implements OnInit {

  project = {} as Project;
  projects: Project[];
  projectId: any;

  constructor(private afStorage: AngularFireStorage, private route: ActivatedRoute, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      } else {
        let params: any = this.route.snapshot.params;
        if (params && params.id) {
          this.projectId = params.id;
        }
      }
    });
  }

  ngOnInit() {
    this.getProjectList();
  }

  getProjectList() {
    this.projectService.getProject().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Project;
      });
      if (this.projects.length == 0) {
        this.router.navigate(['projectnull']);
      } else {
        let sel = this.projects.filter(d => d.id == this.projectId);
        if (sel.length > 0) {
          this.project = sel[0];
        }
      }
    });
  }

  gotoEdit(project) {
    this.router.navigate(['presentation', { itemdata: JSON.stringify(project) }], { skipLocationChange: true });
  }

  save(project){
    this.router.navigate(['project']);
  }
}
