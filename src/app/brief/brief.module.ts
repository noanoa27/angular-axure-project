import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BriefRoutingModule } from './brief-routing.module';
import { BriefPageComponent } from './brief-page/brief-page.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    BriefRoutingModule
  ],
  declarations: [BriefPageComponent]
})
export class BriefModule { }
