import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { Project } from 'src/app/model/project.model';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';
declare var $: any;

@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.css']
})
export class FeedbackPageComponent implements OnInit {

  project = {} as Project;
  projects: Project[];
  projectId: any;
  selectedIndex: any = 0;
  selectedSectionName: any;
  selectedReview: any = [];
  uuid: any;

  selectedType: any = 'Conservation';
  isPreviousEnd: any = false;
  isNextEnd: any = false;


  itemsref: AngularFireList<any>;
  itemsrefLike: AngularFireList<any>;
  itemsref1Like: AngularFireList<any>;
  itemsData: Observable<any[]>;
  likeData: any = [];
  likeDataAll: any = [];

  commentObj: any = {
    comment: '',
    conservation: true,
    improvement: false,
    countLike: 0,
    countDisLike: 0,
  }

  constructor(private fdb: AngularFireDatabase, private route: ActivatedRoute, private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {
    let params: any = this.route.snapshot.params;
    if (params && params.id) {
      this.projectId = params.id;
    } else {
      this.router.navigate(['pin']);
    }
  }

  ngOnInit() {
    this.getProjectData();
  }

  getProjectData() {
    this.projectService.getProject().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Project;
      });
      let sel = this.projects.filter(d => d.id == this.projectId);
      if (sel.length > 0) {
        this.project = sel[0];
        if(this.project.pin == 0){
          $('#endModal').modal('show');
          setTimeout(()=>{
            $('#endModal').modal('hide');
            setTimeout(()=>{
              this.router.navigate(['pin']);
            },800);
          },1500);
          return false;
        }
        this.selectedSectionName = this.project.section[this.selectedIndex].name;
        this.selectedReview = this.project.section[this.selectedIndex].review;
        this.uuid = localStorage.getItem('uuid');
        if (this.uuid && this.uuid != '' && this.uuid != null) {
          this.getLikeDisLikeData();
          this.getAllLikeDisLike();
        } else {
         // this.router.navigate(['pin']);
        }
      }
    });
  }

  getLikeDisLikeData() {
    this.itemsref = this.fdb.list(`review/${this.projectId}/${this.uuid}`);
    this.itemsData = this.itemsref.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      }));
    this.itemsData.subscribe(data => {
      this.likeData = data;
    });
  }

  getAllLikeDisLike() {
    this.itemsref = this.fdb.list(`review/${this.projectId}`);
    this.itemsData = this.itemsref.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      }));
    this.itemsData.subscribe(data => {
      this.likeDataAll = data;
    });
  }

  selectSection(item, index) {
    this.selectedIndex = index;
    this.selectedSectionName = item.name;
    this.selectedReview = item.review;
  }


  selectSection1(flag) {
    if (flag) {
      if (this.selectedIndex != this.project.section.length - 1) {
        this.selectedIndex = this.selectedIndex + 1;
        this.selectedSectionName = this.project.section[this.selectedIndex].name;
        this.selectedReview = this.project.section[this.selectedIndex].review;
        this.isNextEnd = false;
      } else {
        this.isNextEnd = true;
      }
    } else {
      if (this.selectedIndex != 0) {
        this.selectedIndex = this.selectedIndex - 1;
        this.selectedSectionName = this.project.section[this.selectedIndex].name;
        this.selectedReview = this.project.section[this.selectedIndex].review;
        this.isPreviousEnd = false;
      } else {
        this.isPreviousEnd = true;
      }
    }
  }

  addComment() {
    if (this.isInValid(this.commentObj.comment)) {
      this.toastr.error('please enter comment', 'Error', { timeOut: 2000 });
      return false;
    }
    if (this.commentObj.conservation == false && this.commentObj.improvement == false) {
      this.commentObj.conservation = true;
    }
    let len = this.project.section[this.selectedIndex].review.length;
    let obj = JSON.parse(JSON.stringify(this.commentObj));
    this.project.section[this.selectedIndex].review.push(obj);
    this.setLikeDisLike(len);
  }
  
  addComment1() {
    if (this.isInValid(this.commentObj.comment)) {
      this.toastr.error('please enter comment', 'Error', { timeOut: 2000 });
      return false;
    }
    if (this.selectedType == 'Conservation') {
      this.commentObj.conservation = true;
      this.commentObj.improvement = false;
    } else {
      this.commentObj.conservation = false;
      this.commentObj.improvement = true;
    }
    let len = this.project.section[this.selectedIndex].review.length;
    let obj = JSON.parse(JSON.stringify(this.commentObj));
    this.project.section[this.selectedIndex].review.push(obj);
    this.setLikeDisLike(len);
  }

  setLikeDisLike(len) {
    for (let i = 0; i < this.likeDataAll.length; i++) {
      this.likeDataAll[i][this.selectedIndex][len] = { 'isLike': false, 'isDisLike': false };
      this.itemsref1Like = this.fdb.list(`review/${this.projectId}`);
      let key = this.likeDataAll[i].key;
      delete this.likeDataAll[i].key;
      this.itemsref1Like.update(key, this.likeDataAll[i]);
    }
    this.updateComment();
    this.resetComment();
  }

  

  updateComment() {
    this.projectService.sectionUpdate(this.project.section, this.projectId);
  }

  updateLikeData(data) {
    this.itemsrefLike = this.fdb.list(`review/${this.projectId}`);
    this.itemsrefLike.update(this.uuid, data);
  }

  resetComment() {
    this.commentObj.comment = "";
    this.commentObj.improvement = false;
    this.commentObj.conservation = true;
  }

  toggleClick1() {
    this.commentObj.improvement = !this.commentObj.improvement;
    this.commentObj.conservation = false;
  }

  toggleClick() {
    this.commentObj.conservation = !this.commentObj.conservation;
    this.commentObj.improvement = false;
  }

  likeDislike(item) {
    item.isLike = !item.isLike;
    item.isDisLike = false;
    this.updateLikeData(this.likeData);
  }

  likeDislike1(item) {
    item.isDisLike = !item.isDisLike;
    item.isLike = false;
    this.updateLikeData(this.likeData);
  }

  isInValid(outputValue) {
    if (!outputValue || outputValue == "" || outputValue == "0" || outputValue == null || outputValue.length == 0) {
      return true;
    };
  }

  finish() {
    localStorage.clear();
    this.router.navigate(['pin']);
  }

}
