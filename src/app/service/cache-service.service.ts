import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheServiceService {

  authData = {
    id: '',
    uid: '',
    firstname: '',
    lastname: '',
    username: '',
    email: '',
    password: '',
    copassword: '',
    iagree: false,
    aboutus: '',
    photo: '',
  }

  constructor() { }

  setData(data) {
    this.authData.id = data.id;
    this.authData.uid = data.uid;
    this.authData.firstname = data.firstname;
    this.authData.lastname = data.lastname;
    this.authData.username = data.username;
    this.authData.email = data.email;
    this.authData.password = data.password;
    this.authData.copassword = data.copassword;
    this.authData.iagree = data.iagree;
    this.authData.aboutus = data.aboutus == undefined ? "" : data.aboutus;
    this.authData.photo = data.photo == undefined ? "" : data.photo;

  }

  getData() {
    return this.authData;
  }

  setUserName(username) {
    this.authData.username = username;
  }

  setPhoto(photo) {
    this.authData.photo = photo;
  }

}
