import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'src/app/model/user.model';
import { CacheServiceService } from 'src/app/service/cache-service.service';

@Component({
  selector: 'app-forgotpassword-page',
  templateUrl: './forgotpassword-page.component.html',
  styleUrls: ['./forgotpassword-page.component.css']
})
export class ForgotpasswordPageComponent implements OnInit {

  email: any;

  constructor(private router: Router, private fauth: AngularFireAuth, private cacheService: CacheServiceService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  backClick() {
    this.router.navigate(['login']);
  }

  async emailme() {
    try {
      const result = await this.fauth.auth.sendPasswordResetEmail(this.email)
      if (result) {
        this.toastr.success('Reset Link Send', 'Success', { timeOut: 2000 });
      } else {
        this.toastr.success('Reset Link Send', 'Success', { timeOut: 2000 });
      }
    } catch (e) {
      this.toastr.error(e.message, 'Error', { timeOut: 2000 });
    }
  }

}
