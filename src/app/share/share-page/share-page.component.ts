import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { Project } from 'src/app/model/project.model';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-share-page',
  templateUrl: './share-page.component.html',
  styleUrls: ['./share-page.component.css']
})
export class SharePageComponent implements OnInit {

  project = {} as Project;
  projects: Project[];
  projectId: any;
  pin: any;
  isStarted: any = false;
  config: any = { leftTime: 1800, template: '$!m!:$!s!' };

  removeref: AngularFireList<any>;
  itemsref: AngularFireList<any>;
  itemsrefLike: AngularFireList<any>;
  itemsData: Observable<any[]>;
  likeData: any = [];

  constructor(private fdb: AngularFireDatabase, private route: ActivatedRoute, private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      }
    });
    let params: any = this.route.snapshot.params;
    if (params && params.id) {
      this.projectId = params.id;
    }
  }

  ngOnInit() {
    this.getProjectData();
  }



  getProjectData() {
    this.projectService.getProjectData(this.projectId).subscribe((res) => {
      let obj = res.data();
      this.config.leftTime = this.hmsToSecondsOnly(obj.time);
      if (obj.pin == 0) {
        this.pin = Math.floor(Math.random() * (999 - 100 + 1) + 100);
      } else {
        this.pin = obj.pin;
        this.isStarted = true;
      }
    });
  }

  hmsToSecondsOnly(str) {
    if(str == "" || str == null || str == undefined){
      return 0;
    }
    str = str + ':00';
    var p = str.split(':'),
      s = 0, m = 1;
    while (p.length > 0) {
      s += m * parseInt(p.pop(), 10);
      m *= 60;
    }
    return s;
  }

  updatePin() {
    this.projectService.pinUpdate(this.pin, this.projectId);
  }

  updateStatus() {
    this.projectService.statusUpdate('publish', this.projectId);
  }

  start() {
    if (!this.isStarted) {
      this.updatePin();
      this.updateStatus();
      this.isStarted = !this.isStarted;
    } else {
      this.pin = 0;
      this.updatePin();
      this.updateCount();
      this.router.navigate(['summary', { id: this.projectId }]);
    }
  }

  gotopinpage(){
    this.router.navigate(['pin']);
  }

  updateCount() {
    this.projectService.getProject().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Project;
      });
      let sel = this.projects.filter(d => d.id == this.projectId);
      if (sel.length > 0) {
        this.project = sel[0];
        this.getLikeDisLikeData();
      }
    });
  }

  getLikeDisLikeData() {
    this.itemsref = this.fdb.list(`review/${this.projectId}`);
    this.itemsData = this.itemsref.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      }));
    this.itemsData.subscribe(data => {
      if (data.length > 0) {
        let temp = [];
        for (let i = 0; i < this.project.section.length; i++) {
          let nType = {};
          for (let j = 0; j < data.length; j++) {
            if (data[j][i]) {
              for (let key in data[j][i]) {
                if (!nType[key]) {
                  nType[key] = {
                    likecount: 0,
                    dislikecount: 0
                  };
                }
                if (data[j][i][key].isLike) {
                  nType[key]['likecount'] = nType[key]['likecount'] + 1;
                }
                if (data[j][i][key].isDisLike) {
                  nType[key]['dislikecount'] = nType[key]['dislikecount'] + 1;
                }
              }
            }
          }
          for (let l = 0; l < this.project.section[i].review.length; l++) {
            this.project.section[i].review[l].countLike = nType[l].likecount;
            this.project.section[i].review[l].countDisLike = nType[l].dislikecount;
          }

        }
        this.projectService.sectionUpdate(this.project.section, this.projectId);
        setTimeout(() => {
          this.removeref = this.fdb.list(`review`);
          this.removeref.remove(this.projectId);
        }, 5000);
      }
    });
  }
}
