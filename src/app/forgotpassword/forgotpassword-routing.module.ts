import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotpasswordPageComponent } from './forgotpassword-page/forgotpassword-page.component';

const routes: Routes = [
  { path: 'forgot-password', component: ForgotpasswordPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForgotpasswordRoutingModule { }
