export class User {
    id: string;
    uid: string;
    firstname: string;
    lastname: string;
    username: string;
    email: string;
    password: string;
    copassword: string;
    iagree:boolean;
    aboutus:string;
    photo:string;
}
