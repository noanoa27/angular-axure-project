import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharePageComponent } from './share-page/share-page.component';

const routes: Routes = [
  { path: 'share', component: SharePageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShareRoutingModule { }
