import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PinPageComponent } from './pin-page/pin-page.component';

const routes: Routes = [
  { path: 'pin', component: PinPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PinRoutingModule { }
