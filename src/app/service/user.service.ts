import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/model/user.model';
import { CacheServiceService } from 'src/app/service/cache-service.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private firestore: AngularFirestore, private cacheService: CacheServiceService) { }

  getUser() {
    return this.firestore.collection('user').snapshotChanges();
  }

  createUser(user: User) {
    return this.firestore.collection('user').add(user);
  }

  updateUser(user: User) {
    let userId = user.id;
    delete user.id;
    return this.firestore.doc('user/' + userId).update(user);
  }

  getUserData(uid) {
    return this.firestore.collection('user').ref.where('uid', '==', uid).get().then((snapshot) => {
      if (snapshot.empty) {
        return;
      }
      let obj;
      snapshot.forEach((doc) => {
        obj = doc.data();
        obj['id'] = doc.id;
        this.cacheService.setData(obj);
      });
    });
  }
  
  isUserExisting(id) {
    return this.firestore.collection('user').ref.where('uid', '==', id).get();
  }
  isUserExistingByname(name){
    return this.firestore.collection('user').ref.where('username', '==', name).get();
  }

  deleteUser(userId: string) {
    return this.firestore.doc('user/' + userId).delete();
  }

}
