import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedbackPageComponent } from './feedback-page/feedback-page.component';

const routes: Routes = [
  { path: 'feedback', component: FeedbackPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedbackRoutingModule { }
