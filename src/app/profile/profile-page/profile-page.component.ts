import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { User } from 'src/app/model/user.model';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  
  user = {} as User;
  file: File = null;
  isLoading: any = false;
 
  constructor(private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private userService: UserService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService){
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      }else{
        this.userService.getUserData(data.uid).then(data=>{
          this.user = this.cacheService.getData();
        });
      }
    });
  } 

  ngOnInit() {
    
  }

  update(user: User) {
    if (this.isInValid(user.firstname) ||
      this.isInValid(user.lastname) ||
      this.isInValid(user.username)) {
      this.toastr.error('Please enter all required fields.', 'Required', { timeOut: 3000 });
      return false;
    }
    this.isLoading = true;
    if (this.file && this.file != null) {
      const userStorageRef = this.afStorage.upload('/upload/' + user.uid, this.file);
      const file = this.file;
      const filePath = '/upload/' + user.uid;
      const task = this.afStorage.upload(filePath, file).then(() => {
        const ref = this.afStorage.ref(filePath);
        const downloadURL = ref.getDownloadURL().subscribe(url => {
          user.photo = url;
          this.updateUserData(user);
        })
      });
    } else {
      this.updateUserData(user);
    }
  }

  updateUserData(user) {
    this.userService.updateUser(user).then(res => {
      this.isLoading = false;
      this.toastr.success('Profile Updated.', 'Success', { timeOut: 3000 });
      this.userService.getUserData(user.uid).then(data=>{
        this.user = this.cacheService.getData();
      });   
    });
  }


  backClick(){
    this.router.navigate(['project']);
  }
  
  selectImage(event) {
    this.file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (eventCurr: ProgressEvent) => {
        this.user.photo = (<FileReader>eventCurr.target).result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  isInValid(outputValue) {
    if (!outputValue || outputValue == "" || outputValue == "0" || outputValue == null || outputValue.length == 0) {
      return true;
    };
  }

}
