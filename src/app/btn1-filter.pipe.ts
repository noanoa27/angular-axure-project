import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'btn1Filter'
})
export class Btn1FilterPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => {
      if(filter == 'Improvement' && item.improvement){
        return true;
      }else if(filter == 'Conservation' && item.conservation){
        return true;
      }else {
        return false;
      }
    });
  }

}
