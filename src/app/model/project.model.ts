export class Project {
    id: string;
    uid: string;
    title:string;
    brief:string;
    date:string;
    time:string;
    section:any;
    status:string;
    pin:number;
}
