import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textFilter'
})
export class TextFilterPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item =>item.title ? item.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1 : false);
  }
}
