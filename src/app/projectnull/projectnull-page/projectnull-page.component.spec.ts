import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectnullPageComponent } from './projectnull-page.component';

describe('ProjectnullPageComponent', () => {
  let component: ProjectnullPageComponent;
  let fixture: ComponentFixture<ProjectnullPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectnullPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectnullPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
