import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-projectnull-page',
  templateUrl: './projectnull-page.component.html',
  styleUrls: ['./projectnull-page.component.css']
})
export class ProjectnullPageComponent implements OnInit {

  constructor(private fauth: AngularFireAuth, private router: Router) { 
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      }
    });
  }

  ngOnInit() {
  }
  
  createProject(){
    this.router.navigate(['presentation']);
  }

}
