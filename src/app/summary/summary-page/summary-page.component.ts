import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { Project } from 'src/app/model/project.model';

import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.css']
})
export class SummaryPageComponent implements OnInit {

  project = {} as Project;
  projectId: any;
  selectedIndex: any = 0;
  searchtext: any = 'All'
  selectedSectionName: any;
  selectedReview: any = [];
  sortFieldName: any = 'countLike';
  sortflag: any = false;
  selectedType: any = 'Conservation';
  isPreviousEnd:any = false;
  isNextEnd:any = false;

  constructor(private route: ActivatedRoute, private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      }
    });
    let params: any = this.route.snapshot.params;
    if (params && params.id) {
      this.projectId = params.id;
    }
  }

  ngOnInit() {
    this.getProjectData();
  }

  getProjectData() {
    this.projectService.getProjectData(this.projectId).subscribe((res) => {
      let obj = res.data();
      this.project.id = res.id;
      this.project.brief = obj.brief;
      this.project.date = obj.date;
      this.project.section = obj.section;
      this.project.id = obj.time;
      this.project.title = obj.title;
      this.selectedSectionName = obj.section[0].name;
      this.selectedReview = obj.section[0].review;
    });
  }

  selectSection(item, index) {
    this.selectedIndex = index;
    this.selectedSectionName = item.name;
    this.selectedReview = item.review;
  }
}
