import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ForgotpasswordRoutingModule } from './forgotpassword-routing.module';
import { ForgotpasswordPageComponent } from './forgotpassword-page/forgotpassword-page.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    LayoutModule,
    FormsModule,
    ForgotpasswordRoutingModule
  ],
  declarations: [ForgotpasswordPageComponent]
})
export class ForgotpasswordModule { }
