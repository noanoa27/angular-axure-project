import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BriefPageComponent } from './brief-page/brief-page.component';

const routes: Routes = [
  { path: 'brief', component: BriefPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BriefRoutingModule { }
