import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CountdownModule } from 'ngx-countdown';

import { ShareRoutingModule } from './share-routing.module';
import { SharePageComponent } from './share-page/share-page.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    CountdownModule,
    ShareRoutingModule
  ],
  declarations: [SharePageComponent]
})
export class ShareModule { }
