import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { FeedbackRoutingModule } from './feedback-routing.module';
import { FeedbackPageComponent } from './feedback-page/feedback-page.component';
import { BtnFilterPipe } from '../btn-filter.pipe';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    FeedbackRoutingModule
  ],
  declarations: [FeedbackPageComponent,BtnFilterPipe],
  exports: [BtnFilterPipe]
})
export class FeedbackModule { }
