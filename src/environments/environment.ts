// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCmaYyZB4z8X-B07H-HLZIyuQQiCRcIy4U",
    authDomain: "project-b3dfc.firebaseapp.com",
    databaseURL: "https://project-b3dfc.firebaseio.com",
    projectId: "project-b3dfc",
    storageBucket: "",
    messagingSenderId: "773391686693",
    appId: "1:773391686693:web:38085c0a47f3a8a2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
