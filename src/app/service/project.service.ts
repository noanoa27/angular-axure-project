import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Project } from 'src/app/model/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private firestore: AngularFirestore) { }

  getProject() {
    return this.firestore.collection('project').snapshotChanges();
  }

  createProject(project: Project) {
    return this.firestore.collection('project').add(project);
  }

  updateProject(project: Project) {
    let customerId = project.id;
    delete project.id;
    return this.firestore.doc('project/' + customerId).update(project);
  }

  pinUpdate(value, id) {
    var ref = this.firestore.collection('project').doc(id);
    const nType = {};
    nType['pin'] = value;
    let removeSymbols = ref.update(nType);
  }
  
  statusUpdate(value, id) {
    var ref = this.firestore.collection('project').doc(id);
    const nType = {};
    nType['status'] = value;
    let removeSymbols = ref.update(nType);
  }

  sectionUpdate(item, id) {
    var ref = this.firestore.collection('project').doc(id);
    const nType = {};
    nType['section'] = item;
    let removeSymbols = ref.set(nType, { merge: true });
  }

  getProjectFromPin(pin) {
    return this.firestore.collection('project').ref.where('pin', '==', parseInt(pin)).get();
  }

  getProjectData(id) {
    return this.firestore.collection('project').doc(id).get();
  }

  deleteProject(customerId: string) {
    return this.firestore.doc('project/' + customerId).delete();
  }

}
