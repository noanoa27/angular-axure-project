import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { SummaryRoutingModule } from './summary-routing.module';
import { SummaryPageComponent } from './summary-page/summary-page.component';
import { DrpFilterPipe } from '../drp-filter.pipe';
import { SortPipePipe } from '../sort-pipe.pipe';
import { Btn1FilterPipe } from '../btn1-filter.pipe';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    SummaryRoutingModule
  ],
  declarations: [SummaryPageComponent,DrpFilterPipe,SortPipePipe,Btn1FilterPipe],
  exports: [DrpFilterPipe,SortPipePipe,Btn1FilterPipe]
})
export class SummaryModule { }
