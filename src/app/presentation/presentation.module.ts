import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { PresentationRoutingModule } from './presentation-routing.module';
import { PresentationPageComponent } from './presentation-page/presentation-page.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    PresentationRoutingModule
  ],
  declarations: [PresentationPageComponent]
})
export class PresentationModule { }
