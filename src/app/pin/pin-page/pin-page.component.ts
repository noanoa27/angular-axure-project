import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Project } from 'src/app/model/project.model';

import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-pin-page',
  templateUrl: './pin-page.component.html',
  styleUrls: ['./pin-page.component.css']
})
export class PinPageComponent implements OnInit {

  pin: any;
  itemsRef: AngularFireList<any>;



  constructor(private fdb: AngularFireDatabase, private route: ActivatedRoute, private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {

  }

  ngOnInit() {

  }

  getProjectFromPin() {
    if (this.isInValid(this.pin)) {
      this.toastr.error('please enter pin', 'Error', { timeOut: 2000 });
      return false;
    }
    this.projectService.getProjectFromPin(this.pin).then(snapshot => {
      if (snapshot.empty) {
        this.toastr.error('Enter Wrong Pin!', 'Error', { timeOut: 2000 });
        return false;
      }
      snapshot.forEach((doc) => {
        let obj = doc.data();
        this.setUniqueData(doc.id, obj);
        this.router.navigate(['feedback', { id: doc.id}]);
      });
    });
  }

  setUniqueData(id, obj) {
    let uuid = localStorage.getItem('uuid');
    if (uuid && uuid != '' && uuid != null) {
      let key = uuid;
      let temp = [];
      for (let i = 0; i < obj.section.length; i++) {
        let item = obj.section[i];
        let arr = [];
        for (let j = 0; j < item.review.length; j++) {
          arr.push({ 'isLike': false, 'isDisLike': false });
        }
        if(item.review.length == 0){
          arr.push("");
        }
        temp.push(arr);
      }
      this.itemsRef = this.fdb.list(`review/${id}`);
      this.itemsRef.update(key, temp);
    } else {
      let random = Math.floor(Math.random() * (999 - 100 + 1) + 100);
      localStorage.setItem('uuid', this.pin + '-' + random);
      let key = this.pin + '-' + random;
      let temp = [];
      for (let i = 0; i < obj.section.length; i++) {
        let item = obj.section[i];
        let arr = [];
        for (let j = 0; j < item.review.length; j++) {
          arr.push({ 'isLike': false, 'isDisLike': false });
        }
        if(item.review.length == 0){
          arr.push("");
        }
        temp.push(arr);
      }
      this.itemsRef = this.fdb.list(`review/${id}`);
      this.itemsRef.update(key, temp);
    }
  }


  isInValid(outputValue) {
    if (!outputValue || outputValue == "" || outputValue == "0" || outputValue == null || outputValue.length == 0) {
      return true;
    };
  }

}
