import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';

import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { SignupModule } from './signup/signup.module';
import { RegisterModule } from './register/register.module';
import { ForgotpasswordModule } from './forgotpassword/forgotpassword.module';
import { ProjectnullModule } from './projectnull/projectnull.module';
import { PinModule } from './pin/pin.module';
import { ShareModule } from './share/share.module';
import { ProfileModule } from './profile/profile.module';
import { PresentationModule } from './presentation/presentation.module';
import { BriefModule } from './brief/brief.module';
import { FeedbackModule } from './feedback/feedback.module';
import { SummaryModule } from './summary/summary.module';
import { ProjectModule } from './project/project.module';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    HomeModule,
    LoginModule,
    SignupModule,
    RegisterModule,
    ForgotpasswordModule,
    ProjectnullModule,
    PinModule,
    ShareModule,
    ProfileModule,
    PresentationModule,
    BriefModule,
    FeedbackModule,
    SummaryModule,
    ProjectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
