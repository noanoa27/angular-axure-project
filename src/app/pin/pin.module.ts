import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { PinRoutingModule } from './pin-routing.module';
import { PinPageComponent } from './pin-page/pin-page.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    PinRoutingModule
  ],
  declarations: [PinPageComponent]
})
export class PinModule { }
