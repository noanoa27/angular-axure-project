import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { Project } from 'src/app/model/project.model';

import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';
import { UserService } from 'src/app/service/user.service';
import { CompleterService, CompleterData } from 'ng2-completer';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css']
})
export class ProjectPageComponent implements OnInit {

  project = {} as Project;
  projects: Project[];

  selectedId: any = -1;
  searchtext: any = "";

  isLoading:any = true;
  dataService: CompleterData;

  constructor(private completerService: CompleterService, private userService: UserService, private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      }else{
        this.userService.getUserData(data.uid).then(data=>{
          this.isLoading = true;
          this.getProjectList();      
        });
      }
    });
  }

  ngOnInit() {
   
  }

  createProject() {
    this.router.navigate(['presentation']);
  }

  getProjectList() {
    this.projectService.getProject().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Project;
      });
      let sel = this.projects.filter(d => d.uid == this.cacheService.authData.uid);
      if (sel.length > 0) {
        this.projects = sel;
        this.dataService = this.completerService.local(this.projects, 'title', 'title');
        this.isLoading = false;
      } else {
        this.router.navigate(['projectnull']);
        this.isLoading = false;
      }
    });
  }
  
  selectProject(event){
    this.router.navigate(['brief', { id: event.originalObject.id }]);
  }

  reviewClick(project: Project) {
    this.router.navigate(['summary', { id: project.id }]);
  }

  editClick(project: Project) {
    this.router.navigate(['presentation', { itemdata: JSON.stringify(project) }], { skipLocationChange: true });
  }

  shareClick(project: Project) {
    this.router.navigate(['share', { id: project.id }]);
  }

  deleteClick(project: Project) {
    this.selectedId = project.id;
  }

  delete(id: string) {
    this.projectService.deleteProject(id).then(res => {
      this.toastr.success('Project Deleted', 'Success', { timeOut: 2000 });
    });
  }
}
