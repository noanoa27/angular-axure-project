import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupPageComponent } from './signup-page/signup-page.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    LayoutModule,
    SignupRoutingModule
  ],
  declarations: [SignupPageComponent]
})
export class SignupModule { }
