import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { TextFilterPipe } from '../text-filter.pipe';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectPageComponent } from './project-page/project-page.component';
import { Ng2CompleterModule } from "ng2-completer";

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    Ng2CompleterModule,
    ProjectRoutingModule
  ],
  declarations: [ProjectPageComponent, TextFilterPipe],
  exports: [TextFilterPipe]
})
export class ProjectModule { }
