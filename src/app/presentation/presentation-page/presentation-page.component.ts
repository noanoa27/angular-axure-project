import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { Project } from 'src/app/model/project.model';

import { ToastrService } from 'ngx-toastr';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-presentation-page',
  templateUrl: './presentation-page.component.html',
  styleUrls: ['./presentation-page.component.css']
})
export class PresentationPageComponent implements OnInit {

  isError: any = false;
  isEdit: any = false;
  project = {} as Project;
  projects: Project[];

  constructor(private afStorage: AngularFireStorage, private route: ActivatedRoute, private cacheService: CacheServiceService, private projectService: ProjectService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService) {
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.router.navigate(['home']);
      } else {
        let params: any = this.route.snapshot.params;
        if (params && params.itemdata) {
          this.project = JSON.parse(params.itemdata);
          this.isEdit = true;
        } else {
          let nType = {
            name: '',
            review: []
          }
          this.project.section = [];
          this.project.section.push(nType);
        }
      }
    });
  }

  ngOnInit() {

  }

  addMore() {
    let nType = {
      name: '',
      review: []
    }
    this.project.section.push(nType);
  }

  removeItem(item) {
    this.project.section.splice(this.project.section.indexOf(item), 1);
  }

  create(project: Project) {
    this.isError = false;
    if (this.isInValid(project.title)) {
      this.toastr.error('Please enter all required fields.', 'Error', { timeOut: 3000 });
      this.isError = true;
      return false;
    }

    if (project.section) {
      for (let i = 0; i < project.section.length; i++) {
        if (this.isInValid(project.section[i].name)) {
          this.toastr.error('Please enter all required fields.', 'Error', { timeOut: 3000 });
          this.isError = true;
          return false;
        }
      }
    }


    if (this.isEdit) {
      let tempId = project.id;
      if (project.time == "" || project.time == null || project.time == undefined) {
        project.time = "00:30";
      }
      this.projectService.updateProject(project).then(res => {
        this.toastr.success('Project Updated.', 'Success', { timeOut: 2000 });
        this.router.navigate(['brief', { id: tempId }]);
      });
    } else {
      project['pin'] = 0;
      project['uid'] = this.cacheService.authData.uid;
      project['status'] = 'unpublish';
      if (project.time == "" || project.time == null || project.time == undefined) {
        project.time = "00:30";
      }
      this.projectService.createProject(project).then(res => {
        this.toastr.success('Project Created.', 'Success', { timeOut: 2000 });
        this.router.navigate(['brief', { id: res.id }]);
      });
    }
  }

  gotoBrief() {
    this.router.navigate(['brief', { id: this.project.id }]);
  }

  isInValid(outputValue) {
    if (!outputValue || outputValue == "" || outputValue == "0" || outputValue == null || outputValue.length == 0) {
      return true;
    };
  }

}
