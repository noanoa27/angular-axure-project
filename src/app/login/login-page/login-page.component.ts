import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'src/app/model/user.model';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import * as firebase from 'firebase/app';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  user = {} as User;
  isError: any = false;
  isLoading: any = false;

  constructor(private userService: UserService, private router: Router, private fauth: AngularFireAuth, private cacheService: CacheServiceService, private toastr: ToastrService) { }

  ngOnInit() {

  }
  async loginWithUsernamePwd(user: User) {
    try {
      this.isError = false;
      if (this.isInValid(user.email) ||
        this.isInValid(user.password)) {
        this.isError = true;
        return false;
      }
      this.isLoading = true;
      const result = await this.fauth.auth.signInWithEmailAndPassword(user.email, user.password)
      if (result) {
        this.cacheService.setData({ uid: result.user.uid, email: result.user.email });
        this.isLoading = false;
        this.toastr.success('Login Success.', 'Success', { timeOut: 2000 });
        this.router.navigate(['project']);
      } else {
        this.isLoading = false;
        this.toastr.error('Opps..Something Wrong Try Again', 'Error', { timeOut: 2000 });
      }
    } catch (e) {
      this.isLoading = false;
      this.toastr.error(e.message, 'Error', { timeOut: 2000 });
    }
  }

  loginWithGoogle(user: User) {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.fauth.auth
        .signInWithPopup(provider)
        .then(result => {
          if (result) {
            user.uid = result.user.uid;
            user.email = result.user.email;
            user.username = result.additionalUserInfo.profile['name'];
            user.firstname = result.additionalUserInfo.profile['given_name'];
            user.lastname = result.additionalUserInfo.profile['family_name'];
            user.password = '';
            user.copassword = '';
            user.aboutus = '';
            user.photo = '';
            user.iagree = true;
            this.userService.isUserExisting(user.uid).then((snapshot) => {
              if (snapshot.empty) {
                this.userService.createUser(user).then(res => {
                  this.toastr.success('Success.', 'Success', { timeOut: 2000 });
                  resolve(result);
                  this.cacheService.setData({ uid: result.user.uid, email: result.user.email });
                  this.router.navigate(['project']);
                });
              } else {
                this.cacheService.setData({ uid: result.user.uid, email: result.user.email });
                this.router.navigate(['project']);
              }
            });
          }
        }, err => {
          this.toastr.error('Opps..Something Wrong Try Again', 'Error', { timeOut: 2000 });
          reject(err);
        })
    })
  }

  loginWithFacebook(user: User) {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this.fauth.auth
        .signInWithPopup(provider)
        .then(result => {
          user.uid = result.user.uid;
          user.email = result.user.email;
          user.username = result.additionalUserInfo.profile['name'];
          user.firstname = result.additionalUserInfo.profile['first_name'];
          user.lastname = result.additionalUserInfo.profile['last_name'];
          user.password = '';
          user.copassword = '';
          user.aboutus = '';
          user.photo = '';
          user.iagree = true;
          this.userService.isUserExisting(user.uid).then((snapshot) => {
            if (snapshot.empty) {
              this.userService.createUser(user).then(res => {
                this.toastr.success('Success.', 'Success', { timeOut: 2000 });
                resolve(result);
                this.cacheService.setData({ uid: result.user.uid, email: result.user.email });
                this.router.navigate(['project']);
              });
            } else {
              this.cacheService.setData({ uid: result.user.uid, email: result.user.email });
              this.router.navigate(['project']);
            }
          });
        }, err => {
          this.toastr.error('Opps..Something Wrong Try Again', 'Error', { timeOut: 2000 });
          reject(err);
        })
    })
  }
  
  gotoForgotPasswordPage() {
    this.router.navigate(['forgot-password']);
  }

  isInValid(outputValue) {
    if (!outputValue || outputValue == "" || outputValue == "0" || outputValue == null || outputValue.length == 0) {
      return true;
    };
  }

}
