import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { User } from 'src/app/model/user.model';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  isError:any = false;
  isExistingUsername:any = false;
  isInValidEmail:any = false;
  user = {} as User;
  checkpwdStrength = {
    'type': '',
    'margin-left': '12px'
  };

  constructor(private afStorage: AngularFireStorage, private cacheService: CacheServiceService, private userService: UserService, private fauth: AngularFireAuth, private router: Router, private toastr: ToastrService){

  } 

  ngOnInit() {
  }
  
  register(user: User) {
    this.isError = false;
    if (this.isInValid(user.firstname) ||     
      this.isInValid(user.lastname) ||
      this.isInValid(user.username) ||
      this.isInValid(user.email) ||
      this.isInValid(user.password) || 
      this.isInValid(user.copassword)) {
      this.isError = true;
      this.toastr.error('please fill all required field', 'Error', { timeOut: 2000 });
      return false;
    }
    if(this.isExistingUsername){
      this.toastr.error('Username must be unique', 'Error', { timeOut: 2000 });
      return false;
    }
    if(this.isInValidEmail){
      this.toastr.error('Must be in real email format', 'Error', { timeOut: 2000 });
      return false;
    }
    if (user.password != user.copassword) {
      this.toastr.error('password not match', 'Error', { timeOut: 2000 });
      return false;
    }
    if (!user.iagree) {
      this.toastr.error('please agree to the privacy policy', 'Error', { timeOut: 2000 });
      return false;
    }
    this.singup(user)
  }

  checkUsername(user: User){
    this.userService.isUserExistingByname(user.username).then((snapshot) => {
      if (!snapshot.empty) {
        this.isExistingUsername = true;
        return true;
      }else{
        this.isExistingUsername = false;
        return false;
      } 
    });
  }

  validateEmail(user: User) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(user.email) == false) {
        this.isInValidEmail = true;
        return false;
    }else{
      this.isInValidEmail = false;
      return true;
    }
  }

  checkPasswordStroness(user: User) {
    let strongRegularExp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    let mediumRegularExp = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
    if (strongRegularExp.test(user.password)) {
      this.checkpwdStrength["color"] = "green";
      this.checkpwdStrength["type"] = "Strong";
    } else if (mediumRegularExp.test(user.password)) {
      this.checkpwdStrength["color"] = "orange";
      this.checkpwdStrength["type"] = "Medium";
    } else {
      this.checkpwdStrength["color"] = "red";
      this.checkpwdStrength["type"] = "Week";
    }
    if (user.password == "" || user.password == null) {
      this.checkpwdStrength["color"] = "";
      this.checkpwdStrength["type"] = "";
    }
  }

  async singup(user: User) {
    try {
      const result = await this.fauth.auth.createUserWithEmailAndPassword(user.email, user.password)
      if (result) {
        user.uid = result.user.uid;
        this.userService.createUser(user).then(res => {
          this.toastr.success('Sign up Success.', 'Success', { timeOut: 2000 });
          this.router.navigate(['project']);
        });
      } else {
        this.toastr.error('Opps..Something Went Wrong Try Again', 'Error', { timeOut: 2000 });
      }
    } catch (e) {
      this.toastr.error(e.message, 'Error', { timeOut: 2000 });
    }
  }
  
  isInValid(outputValue) {
    if (!outputValue || outputValue == "" || outputValue == "0" || outputValue == null || outputValue.length == 0) {
      return true;
    };
  }

}
