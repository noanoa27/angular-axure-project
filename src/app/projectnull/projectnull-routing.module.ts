import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectnullPageComponent } from './projectnull-page/projectnull-page.component';

const routes: Routes = [
  { path: 'projectnull', component: ProjectnullPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectnullRoutingModule { }
