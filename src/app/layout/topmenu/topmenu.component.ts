import { Component, OnInit } from '@angular/core';
import { style, state, animate, transition, trigger } from '@angular/animations';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CacheServiceService } from 'src/app/service/cache-service.service';
import { UserService } from 'src/app/service/user.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Project } from 'src/app/model/project.model';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-topmenu',
  animations: [
    trigger('expandCollapse', [
      state('close', style({
        'width': '0%',
        'display':'none'
      })),
      state('open', style({
        'width': '100%',
        'display':'flex'
      })),
      transition('open <=> close', animate('400ms ease-in'))
    ])
  ],
  templateUrl: './topmenu.component.html',
  styleUrls: ['./topmenu.component.css']
})
export class TopmenuComponent implements OnInit {

  isLoginMenu: any = false;
  isSignupMenu: any = false;
  isSingoutMenu: any = false;
  isMyProjectMenu: any = false;
  isSearchMenu: any = false;
  isProfileMenu: any = false;
  isPinmenu:any = false;
  authData: any = null;
  isLoging: any = false;
  state: string = 'close';
  searchStr:any = '';
  project = {} as Project;
  projects: Project[];
  dataService: CompleterData;

  constructor(private completerService: CompleterService, private projectService: ProjectService, private userService: UserService, private router: Router, private cacheService: CacheServiceService, private toastr: ToastrService, private fauth: AngularFireAuth) {
    this.fauth.authState.subscribe(data => {
      if (data == null) {
        this.isLoging = false;
      } else {
        this.isLoging = true;
        this.userService.getUserData(data.uid).then(data => {
          this.authData = this.cacheService.getData();
          this.getProjectList();
        });
      }
    });
  }

  ngOnInit() {
    
  }
  selectProject(event){
    this.router.navigate(['brief', { id: event.originalObject.id }]);
  }

  gotoLink(url) {
    this.router.navigate([url]);
  }

  signOut() {
    this.fauth.auth.signOut().then(() => {
      this.toastr.success('Logout Success', 'Success', { timeOut: 2000 });
      this.router.navigate(['home']);
    })
  }

  getProjectList() {
    this.projectService.getProject().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Project;
      });
      let sel = this.projects.filter(d => d.uid == this.cacheService.authData.uid);
      if (sel.length > 0) {
        this.projects = sel;
        this.dataService = this.completerService.local(this.projects, 'title', 'title');
      }
    });
  }

  

  gotoMainPage() {
    if (this.isLoging) {
      this.router.navigate(['project']);
    } else {
      this.router.navigate(['home']);
    }
  }

  searchClick() {
    this.state = (this.state === 'close' ? 'open' : 'close');
  }

  checkMenuVisibility() {
    let path = window.location.pathname;
    if (path == '/login') {
      this.isLoginMenu = false;
      this.isSignupMenu = true;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isPinmenu = false;
    } else if (path == '/home') {
      this.isLoginMenu = true;
      this.isSignupMenu = true;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isPinmenu = true;
    } else if (path == '/signup') {
      this.isLoginMenu = true;
      this.isSignupMenu = false;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isPinmenu = false;
    } else if (path == '/register') {
      this.isLoginMenu = true;
      this.isSignupMenu = true;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isPinmenu = false;
    } else if (path == '/forgot-password') {
      this.isLoginMenu = true;
      this.isSignupMenu = true;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isPinmenu = false;
    } else if (path == '/projectnull') {
      this.isLoginMenu = false;
      this.isSignupMenu = false;
      this.isSingoutMenu = true;
      this.isMyProjectMenu = true;
      this.isSearchMenu = true;
      this.isProfileMenu = true;
      this.isPinmenu = false;
    } else if (path == '/pin') {
      this.isLoginMenu = true;
      this.isSignupMenu = true;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isSearchMenu = false;
      this.isProfileMenu = false;
      this.isPinmenu = false;
    } else if (path == '/share' || path == '/profile' || path == '/presentation' || path == '/brief' || path == '/summary' || path == '/project') {
      this.isLoginMenu = false;
      this.isSignupMenu = false;
      this.isSingoutMenu = true;
      this.isMyProjectMenu = true;
      this.isSearchMenu = true;
      this.isProfileMenu = true;
      this.isPinmenu = false;
    } else if (path.indexOf("brief") > -1 || path.indexOf("summary") > -1 || path.indexOf("share") > -1) {
      this.isLoginMenu = false;
      this.isSignupMenu = false;
      this.isSingoutMenu = true;
      this.isMyProjectMenu = true;
      this.isSearchMenu = true;
      this.isProfileMenu = true;
      this.isPinmenu = false;
    } else if (path.indexOf("feedback") > -1) {
      this.isLoginMenu = true;
      this.isSignupMenu = true;
      this.isSingoutMenu = false;
      this.isMyProjectMenu = false;
      this.isSearchMenu = false;
      this.isProfileMenu = false;
      this.isPinmenu = false;
    }
    return false;
  }

}
